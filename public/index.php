<?php

use LexiSoft\CiCD\MyClass;

require __DIR__ . '/../vendor/autoload.php';

MyClass::shout('Hello world!');
MyClass::shout(null);